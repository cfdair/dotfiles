set -euo pipefail

function install_brew {
  echo "Installing brew..."
  /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
}

[ -x "$(command -v brew)" ] || echo install_brew

echo "Installing dependencies with brew..."
brew bundle

[ -x "$(command -v docker)" ] || (echo "-----------------------------" && read -p "Please ensure docker and docker compose is installed. When installed, press any key to continue...")

echo
echo "-----------------------------"
read -p "Did you want to allow the direnv for this directory? (y/n)" -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
    direnv allow
fi

echo "Done."
