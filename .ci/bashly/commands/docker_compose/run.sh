## -------- Set common envvars -------------------------------
DRY_RUN=
[ ${args[--dryrun]-} ] && DRY_RUN=echo

HELP=
[ ${args[--help_]-} ] && HELP=--help
## ------------------------------------------------------------

echo "Running docker compose run..."

[ ${args[--dryrun]-} ] || set -x
${DRY_RUN} docker compose run --rm ${args[image_name]} ${HELP} ${other_args[*]}
set +x
