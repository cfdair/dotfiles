## -------- Set common envvars -------------------------------
DOCKER_COMPOSE="docker compose run --rm bashly"
[ ${args[--use-local]-} ] && DOCKER_COMPOSE=

DRY_RUN=
[ ${args[--dryrun]-} ] && DRY_RUN=echo

HELP=
[ ${args[--help_]-} ] && HELP=--help
## ------------------------------------------------------------

echo "Building the bashly artefacts..."

[ ${args[--dryrun]-} ] || set -x
${DRY_RUN} ${DOCKER_COMPOSE} generate
${DRY_RUN} rm -rf .ci/docs
${DRY_RUN} ${DOCKER_COMPOSE} render :markdown .ci/docs
set +x
