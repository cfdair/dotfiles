brew update
brew upgrade
pipx upgrade-all
if is_linux_os; then
  sudo apt upgrade
fi
pip install --upgrade pip
