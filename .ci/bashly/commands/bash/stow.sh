set -euxo pipefail


# TODO: Import these from a source of truth
XDG_CONFIG_HOME="${HOME}/.config"
DOTFILES_CONFIG_DIR="${HOME}/.dotfiles/config"

xdg_not_supported=(
  "bash"
)

function run_stow {
  package_name="${1}"
  dir="${2}"
  # XDG not supported
  if [[ "${package_name}" = 'bash' || "${package_name}" = 'vim' ]]; then
    target_dir="${HOME}"
  else
    target_dir="${XDG_CONFIG_HOME}/${package_name}"
  fi
  mkdir -p "${target_dir}"
  stow --dir "${dir}" --target "${target_dir}" "${package_name}"
}

function run_stow_for_dir {
  dir="${1}/config"
  if [[ -d "${dir}" ]]; then
    for package_name in $(find "${dir}" -maxdepth 1 -mindepth 1 -type d | xargs -I{} basename {} | sort); do
      run_stow "${package_name}" "${dir}"
    done
  fi
  dir="${1}/sensitive"
  if [[ -d "${dir}" ]]; then
    for package_name in $(find "${dir}" -maxdepth 1 -mindepth 1 -type d | xargs -I{} basename {} | sort); do
      run_stow "${package_name}" "${dir}"
    done
  fi
}

function friendly_mac_name {
  awk '/SOFTWARE LICENSE AGREEMENT FOR macOS/' '/System/Library/CoreServices/Setup Assistant.app/Contents/Resources/en.lproj/OSXSoftwareLicense.rtf' | awk -F 'macOS ' '{print $NF}' | awk '{print substr($0, 0, length($0)-1)}' | tr '[:upper:]' '[:lower:]'
}

function friendly_linux_distribution_name {
  cat /etc/lsb-release | grep DISTRIB_ID | awk -F= '{printf $2}' | tr '[:upper:]' '[:lower:]'
}

function friendly_linux_release_name {
  cat /etc/lsb-release | grep DISTRIB_CODENAME | awk -F= '{printf $2}' | tr '[:upper:]' '[:lower:]'
}

function stow_etc {
  sudo mkdir -p /etc/zsh
  [[ -L /etc/zsh/zshenv ]] || sudo ln -s "${DOTFILES_CONFIG_DIR}/etc/zsh/zshenv" /etc/zsh/zshenv
  [[ -L /etc/zshenv ]] || sudo ln -s "${DOTFILES_CONFIG_DIR}/etc/zsh/zshenv" /etc/zshenv
}

stow_etc
# TODO: Add hierarchical config store
# base
# mac/mac_version/home
# linux/ubuntu/23.02/home
# This would enable a series of escalating storing of configs.
#
# First, a stow import(?? command) will import some configs from ~/.config/
#
# Then, a promote would escalate that package to a selectable level above into the ../config/package_name path.
#
# So then the stow mechanism will load the hierarchical config paths and then stow each of them.
run_stow_for_dir "${DOTFILES_CONFIG_DIR}"
if is_mac_os; then
  run_stow_for_dir "${DOTFILES_CONFIG_DIR}/mac/$(friendly_mac_name)/${FARMLAND_ESTATE_DEVICE_NAME}"
  run_stow_for_dir "${DOTFILES_CONFIG_DIR}/mac/$(friendly_mac_name)"
  run_stow_for_dir "${DOTFILES_CONFIG_DIR}/mac"
elif is_linux_os; then
  run_stow_for_dir "${DOTFILES_CONFIG_DIR}/linux/$(friendly_linux_distribution_name)/$(friendly_linux_release_name)/${FARMLAND_ESTATE_DEVICE_NAME}"
  run_stow_for_dir "${DOTFILES_CONFIG_DIR}/linux/$(friendly_linux_distribution_name)/$(friendly_linux_release_name)"
  run_stow_for_dir "${DOTFILES_CONFIG_DIR}/linux/$(friendly_linux_distribution_name)"
  run_stow_for_dir "${DOTFILES_CONFIG_DIR}/linux"
fi
