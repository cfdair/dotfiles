#!/usr/bin/env bash

set -euo pipefail

echo "Installing an SSH server..."

apt update

apt install -y openssh-server

echo "Starting server..."
systemctl enable ssh

echo "Done."
