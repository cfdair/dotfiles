#!/usr/bin/env bash

set -euo pipefail

echo "Installing coding dependencies..."

# Install Atom dependencies
apt install -y \
  software-properties-common \
  apt-transport-https \
  wget

# Add Atom repository
echo "Adding atom repo"
wget -q https://packagecloud.io/AtomEditor/atom/gpgkey -O- | apt-key add -
add-apt-repository "deb [arch=amd64] https://packagecloud.io/AtomEditor/atom/any/ any main"

# Load repo data
apt update

apt install -y \
  awscli \
  atom \
  curl \
  firefox
