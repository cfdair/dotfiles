#!/usr/bin/env bash

set -euo pipefail

# All the following will be created only accessible by root.
WIREGUARD_DIR=/etc/wireguard
WIREGUARD_SERVER_CONF="${WIREGUARD_DIR}/wg0.conf"
WIREGUARD_SERVER_PUBLIC_KEY="${WIREGUARD_DIR}/server.pub"

echo "Generating Sam Mac config..."
WIREGUARD_SAM_MAC_DIR=~/.dotfiles/wireguard/sam_mac
WIREGUARD_SAM_MAC_CONF="${WIREGUARD_SAM_MAC_DIR}/sam_mac.conf"
WIREGUARD_SAM_MAC_PRIVATE_KEY="${WIREGUARD_SAM_MAC_DIR}/sam_mac.key"
WIREGUARD_SAM_MAC_PUBLIC_KEY="${WIREGUARD_SAM_MAC_DIR}/sam_mac.pub"
WIREGUARD_SAM_MAC_PRE_SHARED_KEY="${WIREGUARD_SAM_MAC_DIR}/sam_mac.psk"
mkdir -p "${WIREGUARD_SAM_MAC_DIR}"
cd "${WIREGUARD_SAM_MAC_DIR}"
umask 077
wg genkey > "${WIREGUARD_SAM_MAC_PRIVATE_KEY}"
wg pubkey < "${WIREGUARD_SAM_MAC_PRIVATE_KEY}" > "${WIREGUARD_SAM_MAC_PUBLIC_KEY}"
wg genpsk > "${WIREGUARD_SAM_MAC_PRE_SHARED_KEY}"

# This is the local IP of the wireguard server
#
# This could be configured to be a home network DNS hostname if that works.
WIREGUARD_SERVER_IP="192.168.1.19"
WIREGUARD_SERVER_PORT="61029"

echo "Creating config in ${WIREGUARD_SAM_MAC_CONF}..."
cat > "${WIREGUARD_SAM_MAC_CONF}"<< EOF
[Interface]
Address = 10.254.0.2/32
PrivateKey = $(cat ${WIREGUARD_SAM_MAC_PRIVATE_KEY})

[Peer]
Endpoint = ${WIREGUARD_SERVER_IP}:${WIREGUARD_SERVER_PORT}
AllowedIPs = 10.254.0.0/24
PublicKey = $(cat ${WIREGUARD_SERVER_PUBLIC_KEY})
PresharedKey = $(cat ${WIREGUARD_SAM_MAC_PRE_SHARED_KEY})
EOF

sudo cat >> "${WIREGUARD_SERVER_CONF}"<< EOF
[Peer]
Endpoint = ${WIREGUARD_SERVER_IP}:61029
AllowedIPs = 10.254.0.2/32
PublicKey = $(cat ${WIREGUARD_SAM_MAC_PUBLIC_KEY})
PresharedKey = $(cat ${WIREGUARD_SAM_MAC_PRE_SHARED_KEY})
EOF
