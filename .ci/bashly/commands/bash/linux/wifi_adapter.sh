#!/usr/bin/env bash

# From https://waynekhan.github.io/2020/05/30/asus-usb-ac53-nano-ubuntu.html
set -euo pipefail
set -x
echo "Installing wifi adapter usb driver..."

sudo apt-get update
sudo apt-get install build-essential dkms git
git clone https://github.com/cilynx/rtl88x2bu || echo "Already have pulled repo"

cd rtl88x2bu
git pull
VER=$(sed -n 's/\PACKAGE_VERSION="\(.*\)"/\1/p' dkms.conf)
sudo rsync -rvhP ./ /usr/src/rtl88x2bu-${VER}
sudo dkms add -m rtl88x2bu -v ${VER}
sudo dkms build -m rtl88x2bu -v ${VER}
sudo dkms install -m rtl88x2bu -v ${VER}
sudo modprobe 88x2bu
