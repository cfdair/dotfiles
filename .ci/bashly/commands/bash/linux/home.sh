#!/usr/bin/env bash

set -euo pipefail

echo "Installing home dependencies..."

# Install private-internet-access dependencies
apt install -y \
  curl \
  network-manager-openvpn-gnome

# Add steam repository
add-apt-repository multiverse

# Load repo data
apt update

apt install -y \
  firefox \
  qbittorrent \
  steam \
  protonmail-bridge \
  espeak

snap install signal-desktop
snap install spotify
snap install telegram-desktop
snap install telegram-cli
snap install whatsdesk

# Install Sticky notes
sudo apt install -y \
  xpad

# TODO Install obsidian with snap
# Install AppImage apps
# mkdir -p "$APPS"
# wget -O "$APPS/Obsidian-0.8.12.AppImage" https://github.com/obsidianmd/obsidian-releases/releases/download/v0.8.12/Obsidian-0.8.12.AppImage

# Install NordVPN
sh <(curl -sSf https://downloads.nordcdn.com/apps/linux/install.sh)
curl -o /tmp/nordvpn-release_1.0.0_all.deb https://repo.nordvpn.com/deb/nordvpn/debian/pool/main/nordvpn-release_1.0.0_all.deb
sudo apt-get install /tmp/nordvpn-release_1.0.0_all.deb
sudo rm -f /tmp/nordvpn-release_1.0.0_all.deb
sudo apt-get update

# Install protonmail bridge
curl -o /tmp/protonmail-bridge_1.8.10-1_amd64.deb https://protonmail.com/download/bridge/protonmail-bridge_1.8.10-1_amd64.deb
sudo apt-get install /tmp/protonmail-bridge_1.8.10-1_amd64.deb
