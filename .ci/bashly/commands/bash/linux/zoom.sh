#!/usr/bin/env bash

set -euo pipefail

echo "Installing zoom malware..."
# detailed here: https://ar.al/2020/06/25/how-to-use-the-zoom-malware-safely-on-linux-if-you-absolutely-have-to/

echo "Installing security detail..."
sudo apt install -y \
  firejail \
  firejail-profiles \
  apparmor-utils

sudo aa-enforce firejail-default

TEMP_DEB="$(mktemp).deb" && \
  wget -O "$TEMP_DEB" 'https://zoom.us/client/latest/zoom_amd64.deb' && \
  sudo apt install -y "$TEMP_DEB" && \
  rm -f "$TEMP_DEB"

echo "setting up zoom jail..."

# Ensure local settings folder exists for Firejail.
mkdir -p ~/.config/firejail

mkdir -p ~/.zoom

# Add the custom settings to it.
echo "protocol unix,inet,inet6,netlink\nignore seccomp\nseccomp \x21chroot" > ~/.config/firejail/zoom.local

sudo rm /usr/bin/zoom
sudo me=$HOME bash -c 'echo -e "#!/bin/bash\nfirejail --apparmor --profile=/etc/firejail/zoom.profile --private=$me/.zoom /opt/zoom/ZoomLauncher" > /usr/bin/zoom'
