#!/usr/bin/env bash

set -euo pipefail

echo "Installing linuxbrew deps..."
apt install -y \
  build-essential \
  curl \
  file \
  git \
  ruby
