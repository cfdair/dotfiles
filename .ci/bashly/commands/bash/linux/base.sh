#!/usr/bin/env bash

set -euo pipefail

echo "Installing base dependencies..."

# Install base dependencies
apt install -y \
  software-properties-common \
  apt-transport-https \
  wget \
  curl \
  git \
  gnome-tweaks

echo "Installing security detail..."
apt install -y \
  firejail \
  firejail-profiles \
  apparmor-utils

aa-enforce firejail-default

echo "Installing webcam..."
add-apt-repository ppa:openrazer/stable
apt update
apt install -y openrazer-meta guvcview

echo "Installing interface that will eventually include the razer kiyo..."
echo 'deb http://download.opensuse.org/repositories/hardware:/razer/xUbuntu_20.04/ /' | tee /etc/apt/sources.list.d/hardware:razer.list
curl -fsSL https://download.opensuse.org/repositories/hardware:razer/xUbuntu_20.04/Release.key | gpg --dearmor | tee /etc/apt/trusted.gpg.d/hardware_razer.gpg > /dev/null
apt update
apt install -y razergenie

echo "Installing 1password"
curl -sS https://downloads.1password.com/linux/keys/1password.asc | gpg --dearmor --output /usr/share/keyrings/1password-archive-keyring.gpg
echo 'deb [arch=amd64 signed-by=/usr/share/keyrings/1password-archive-keyring.gpg] https://downloads.1password.com/linux/debian/amd64 stable main' | tee /etc/apt/sources.list.d/1password.list
mkdir -p /etc/debsig/policies/AC2D62742012EA22/
curl -sS https://downloads.1password.com/linux/debian/debsig/1password.pol | tee /etc/debsig/policies/AC2D62742012EA22/1password.pol
mkdir -p /usr/share/debsig/keyrings/AC2D62742012EA22
curl -sS https://downloads.1password.com/linux/keys/1password.asc | gpg --dearmor --output /usr/share/debsig/keyrings/AC2D62742012EA22/debsig.gpg
apt update && apt install 1password 1password-cli
groupadd onepassword-cli
chown root:onepassword-cli /bin/op && chmod g+s /bin/op
mv /bin/op /usr/local/bin/op

echo "Installing keyboard bindings..."
apt-get install -y xbindkeys xbindkeys-config xvkbd
