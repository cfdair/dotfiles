#!/usr/bin/env bash

set -euo pipefail

echo "Installing docker..."

HOME_USER=samsonite

# Load repo data
apt update

apt install -y docker.io

systemctl enable --now docker

usermod -aG docker "$HOME_USER"

setfacl -m "user:${HOME_USER}:rw" /var/run/docker.sock
