# Need to use the system python, so that means either uninstalling
# mise python and brew python, or something smarter here.
#
# Specifically, uninstall brew python with:
# brew uninstall python3 cairo glib gobject-introspection
# and uninstall mise python with:
# mise uninstall python --all
#
# Finally, to install kinto, run:
# /bin/bash -c "$(wget -qO- https://raw.githubusercontent.com/rbreaves/kinto/HEAD/install/linux.sh || curl -fsSL https://raw.githubusercontent.com/rbreaves/kinto/HEAD/install/linux.sh)"

sudo apt update
sudo apt install -y \
  gcc \
  gobject-introspection \
  gir1.2-gtk-3.0 \
  libcairo2-dev \
  libgirepository1.0-dev \
  pkg-config \
  python3-cairo \
  python3-click \
  python3-dev \
  python3-gi \
  python3-gi-cairo \
  gnome-tweaks \
  gnome-shell-extension-appindicator \
  gir1.2-appindicator3-0.1
brew install cairo bzip2
pip3 install --break-system-packages --user pycairo
brew install gobject-introspection

pip install PyGObject
brew install libffi
~/.config/kinto/gui/kinto-gui.py
