#!/usr/bin/env bash

set -euo pipefail

echo "Preparing linux ubuntu for use..."

apt update

apt install -y build-essential
