#!/usr/bin/env bash

set -euo pipefail

echo "Setting up mouse to not auto suspend..."
sudo su
echo 'ACTION=="add", SUBSYSTEM="bluetooth" ATTR{power/autosuspend}="-1"' >> /etc/udev/rules.d/50-usb_power_save.rules
echo 'on' > /sys/bus/usb/devices/usb1/power/control
udevadm control --reload
exit 0
echo "Done."
