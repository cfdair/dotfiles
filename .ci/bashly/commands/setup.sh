#!/usr/bin/env bash

set -euo pipefail

cd "$(dirname "${BASH_SOURCE[0]}")"

METADATA_PATH=outputs/farmland_estate_metadata.json


function set_farmland_estate_name {
  options=( $(find ".ci/bashly/commands/farmland/estates" -name \*.sh | awk -F. '{print $1}' | awk -F/ '{print $5 "/" $6}' ) )
  prompt="Choice an estate to build:"

  PS3="$prompt "
  select opt in "${options[@]}" "Quit" ; do
      if (( REPLY == 1 + ${#options[@]} )) ; then
          fail

      elif (( REPLY > 0 && REPLY <= ${#options[@]} )) ; then
          break

      else
          echo "Invalid option. Try another one."
      fi
  done
  echo "${opt}"
}

function set_farmland_estate_installer {
  options=( "zeus" "bash" )
  prompt="Choice an installer:"
  PS3="$prompt "
  select installer_opt in "${options[@]}" "Quit" ; do
      if (( REPLY == 1 + ${#options[@]} )) ; then
          fail

      elif (( REPLY > 0 && REPLY <= ${#options[@]} )) ; then
          break

      else
          echo "Invalid option. Try another one."
      fi
  done
  echo "${installer_opt}"
}

function write_farmland_estate_metadata {
  name="${1}"
  installer="${2}"
  jq -n \
    --arg name "$name" \
    --arg installer "$installer" \
    '{name: $name, installer: $installer}' \
    > "${METADATA_PATH}"
}

function get_saved_farmland_estate_installer {
  cat "${METADATA_PATH}" 2> /dev/null | jq -r '.installer'
}

function get_saved_farmland_estate_name {
  cat "${METADATA_PATH}" 2> /dev/null | jq -r '.name'
}

## Make directory for artefacts that can be deleted at anytime
mkdir -p outputs

farmland_estate_name="$(get_saved_farmland_estate_name || set_farmland_estate_name)"
# Make this import cleaner
export FARMLAND_ESTATE_NAME="${farmland_estate_name}"
export FARMLAND_ESTATE_DEVICE_NAME="$(basename "${farmland_estate_name}")"
farmland_estate_installer="$(get_saved_farmland_estate_installer || set_farmland_estate_installer)" 2> /dev/null

# TODO: Update this to be entered and saved somewhere:
GIT_USER_EMAIL=cf.d.air+gitlab@pm.me
GIT_USER_NAME=cfdair

source ".ci/bashly/commands/farmland/estates/${farmland_estate_name}.sh"

ci_bash_stow_command
ci_bash_update_all_command
ci_bash_prune_all_command

write_farmland_estate_metadata "${farmland_estate_name}" "${farmland_estate_installer}"
