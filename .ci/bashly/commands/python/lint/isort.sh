## -------- Set common envvars -------------------------------
DOCKER_COMPOSE="docker compose run --rm dbt"
[ ${args[--use-local]-} ] && DOCKER_COMPOSE=

DRY_RUN=
[ ${args[--dryrun]-} ] && DRY_RUN=echo

HELP=
[ ${args[--help_]-} ] && HELP=--help
## ------------------------------------------------------------

echo "Running isort linting..."

[ ${args[--dryrun]-} ] || set -x
${DRY_RUN} ${DOCKER_COMPOSE} isort ${PYTHON_CODE_DIRS} ${HELP} --check --diff ${other_args[*]}
set +x