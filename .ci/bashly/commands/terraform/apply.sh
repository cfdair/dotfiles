## -------- Set common envvars -------------------------------
DOCKER_COMPOSE="docker compose run --rm terraform"
[ ${args[--use-local]-} ] && DOCKER_COMPOSE=

DRY_RUN=
[ ${args[--dryrun]-} ] && DRY_RUN=echo

HELP=
[ ${args[--help_]-} ] && HELP=--help
## ------------------------------------------------------------

echo "Running terraform apply..."

[ ${args[--dryrun]-} ] || set -x
${DRY_RUN} ${DOCKER_COMPOSE} terraform -chdir=terraform/airflow/${args[airflow_environment]} apply ${HELP} -out=terraform.tfplan ${other_args[*]}
set +x
