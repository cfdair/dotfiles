#!/usr/bin/env bash

set -euo pipefail

cd "$(dirname "$0")"


is_mac_os && brew_cask_install obsidian
is_linux_os && log_once "No Note taking setup for linux"

log_once "obsidian install is finished..."
