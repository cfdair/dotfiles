#!/usr/bin/env bash

set -euo pipefail

cd "$(dirname "$0")"

source ./shared.sh

brew_install direnv
brew_install gnupg
brew_install tree
brew_install vim
brew_install google-chrome

# For linux
# # Permits time dependent screen temperature
brew_install redshift
brew_install xclip
brew_install docker-compose
# #### pyenv deps
brew_install bzip2
brew_install libffi
brew_install libxml2
brew_install libxmlsec1
brew_install openssl
brew_install readline
brew_install sqlite
brew_install xz
brew_install zlib
