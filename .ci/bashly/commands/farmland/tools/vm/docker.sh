#!/usr/bin/env bash

set -euo pipefail

cd "$(dirname "$0")"


brew_install docker
brew_install docker-machine
if is_linux_os; then
  sudo apt update -y
  sudo apt-get install docker-compose-v2 -y
elif is_mac_os; then
  brew_install docker-compose
fi

log_once "docker install is finished..."
