#!/usr/bin/env bash

set -euo pipefail

cd "$(dirname "$0")"


brew_install docker-machine
if is_linux_os; then
  log_once "No orbstack candidate for linux..."
elif is_mac_os; then
  brew_install orbstack
fi

log_once "orbstack install is finished..."
