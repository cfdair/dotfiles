#!/usr/bin/env bash

set -euo pipefail

cd "$(dirname "$0")"


brew_install nnn

sh -c "$(curl -Ls https://raw.githubusercontent.com/jarun/nnn/master/plugins/getplugs)"

# https://github.com/jarun/nnn/blob/master/plugins/suedit
# https://github.com/jarun/nnn/blob/master/plugins/rsynccp
# https://github.com/jarun/nnn/blob/master/plugins/pdfread
# https://github.com/jarun/nnn/blob/master/plugins/oldbigfile
# https://github.com/jarun/nnn/blob/master/plugins/fzopen
# https://github.com/jarun/nnn/blob/master/plugins/fzhist
# https://github.com/jarun/nnn/blob/master/plugins/fzcd
# https://github.com/jarun/nnn/blob/master/plugins/fixname
# https://github.com/jarun/nnn/blob/master/plugins/finder
#
# # Setup ENVVARS for nnn
# https://github.com/jarun/nnn/tree/master/plugins#nnn-plugins
#
# # Setu pquick start
# https://github.com/jarun/nnn/tree/master/plugins#nnn-plugins

log_once "nnn install is finished..."
