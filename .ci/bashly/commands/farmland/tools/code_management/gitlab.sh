#!/usr/bin/env bash

set -euo pipefail

cd "$(dirname "$0")"


brew_install glab

log_once "gitlab install is finished..."
