#!/usr/bin/env bash

set -euo pipefail

cd "$(dirname "$0")"


brew_install tmuxinator
is_mac_os && brew_install reattach-to-user-namespace
is_linux_os && log_once "No reattach-to-user-namespace for linux"

log_once "tmux install is finished..."
