#!/usr/bin/env bash

set -euo pipefail

cd "$(dirname "$0")"

[[ -d ~/.config/git ]] || mkdir ~/.config/git

brew_install git
brew_install lazygit

# TODO: Import these from a source of truth
export XDG_CONFIG_HOME="${HOME}/.config"

# Git config
git config --file "${XDG_CONFIG_HOME}/git/config" user.email "${GIT_USER_EMAIL}"
git config --file "${XDG_CONFIG_HOME}/git/config" user.name "${GIT_USER_NAME}"


# diff-so-fancy
# =============================
# Install
brew_install diff-so-fancy

# Config
git config --file "${XDG_CONFIG_HOME}/git/config" core.pager "diff-so-fancy | less --tabs=4 -RF"
git config --file "${XDG_CONFIG_HOME}/git/config" interactive.diffFilter "diff-so-fancy --patch"

# Colour
git config --file "${XDG_CONFIG_HOME}/git/config" color.ui true

git config --file "${XDG_CONFIG_HOME}/git/config" color.diff-highlight.oldNormal    "red bold"
git config --file "${XDG_CONFIG_HOME}/git/config" color.diff-highlight.oldHighlight "orange bold 52"
git config --file "${XDG_CONFIG_HOME}/git/config" color.diff-highlight.newNormal    "green bold"
git config --file "${XDG_CONFIG_HOME}/git/config" color.diff-highlight.newHighlight "blue bold 22"

git config --file "${XDG_CONFIG_HOME}/git/config" color.diff.meta       "11"
git config --file "${XDG_CONFIG_HOME}/git/config" color.diff.frag       "magenta bold"
git config --file "${XDG_CONFIG_HOME}/git/config" color.diff.func       "146 bold"
git config --file "${XDG_CONFIG_HOME}/git/config" color.diff.commit     "yellow bold"
git config --file "${XDG_CONFIG_HOME}/git/config" color.diff.old        "red bold"
git config --file "${XDG_CONFIG_HOME}/git/config" color.diff.new        "green bold"
git config --file "${XDG_CONFIG_HOME}/git/config" color.diff.whitespace "red reverse"

log_once "git install is finished..."
