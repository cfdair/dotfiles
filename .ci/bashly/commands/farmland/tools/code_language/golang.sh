#!/usr/bin/env bash

set -euo pipefail

cd "$(dirname "$0")"


brew_install mise

mise install golang

brew_install exercism
brew_install goland

log_once "golang install is finished..."
