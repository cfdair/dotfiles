#!/usr/bin/env bash

set -euo pipefail

cd "$(dirname "$0")"


function is_already_installed_via_pipx() {
  package=$1
  pipx list --short | grep -q "${package}"
}

brew_install mise

mise install python

mise use --global python

if is_mac_os; then
  brew_install pipx
elif is_linux_os; then
  sudo apt update
  sudo apt install pipx
else
  echo "Unrecognised OS when installing tools/code_language/python.sh...";
  exit 1
fi

is_already_installed_via_pipx poetry || pipx install poetry

is_mac_os && brew_cask_install pycharm-ce
is_linux_os && log_once "No Pycharm CE setup for linux"

log_once "python install is finished..."
