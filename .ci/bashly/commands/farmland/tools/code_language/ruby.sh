#!/usr/bin/env bash

set -euo pipefail

cd "$(dirname "$0")"


brew_install mise

mise install ruby

mise use --global ruby

log_once "ruby install is finished..."
