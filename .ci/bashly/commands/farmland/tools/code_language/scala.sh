#!/usr/bin/env bash

set -euo pipefail

cd "$(dirname "$0")"


brew_install mise

mise install scala

mise use --global scala

is_mac_os && brew_cask_install intellij-idea-ce
is_linux_os && log_once "No IntelliJ Idea CE setup for linux"

log_once "scala install is finished..."
