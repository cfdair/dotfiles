#!/usr/bin/env bash

set -euo pipefail

cd "$(dirname "$0")"


brew_install stow

log_once "stow install is finished..."
