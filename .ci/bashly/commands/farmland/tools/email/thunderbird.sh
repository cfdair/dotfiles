#!/usr/bin/env bash

set -euo pipefail

cd "$(dirname "$0")"


is_linux_os && brew_cask_install thunderbird

log_once "thunderbird install is finished..."
