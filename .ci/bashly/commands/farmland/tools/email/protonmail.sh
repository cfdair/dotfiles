#!/usr/bin/env bash

set -euo pipefail

cd "$(dirname "$0")"


is_mac_os && brew_cask_install proton-mail
if is_linux_os; then
  TEMP_DEB="$(mktemp).deb" && \
    wget -O "$TEMP_DEB" 'https://proton.me/download/mail/linux/ProtonMail-desktop-beta.deb' && \
    sudo apt install -y "$TEMP_DEB" && \
    rm -f "$TEMP_DEB"
fi

log_once "protonmail install is finished..."
