#!/usr/bin/env bash

set -euo pipefail

cd "$(dirname "$0")"


is_mac_os && brew_cask_install slack
is_linux_os && log_once "No Slack setup for linux"

log_once "slack install is finished..."
