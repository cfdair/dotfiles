#!/usr/bin/env bash

set -euo pipefail

cd "$(dirname "$0")"


brew_install awscli

is_linux_os && brew_install aws-vault
is_mac_os && brew_cask_install aws-vault

log_once "AWS install is finished..."
