#!/usr/bin/env bash

set -euo pipefail

cd "$(dirname "$0")"


brew_install helix

log_once "helix install is finished..."
