#!/usr/bin/env bash

set -euo pipefail

cd "$(dirname "$0")"


brew_cask_install atom

packages=(
  language-markdown
  language-scala
  language-terraform
  package-settings
)

for package in ${packages[@]}; do
  apm install $package
done

log_once "atom install is finished..."
