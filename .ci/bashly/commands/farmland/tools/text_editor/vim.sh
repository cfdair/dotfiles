#!/usr/bin/env bash

set -euo pipefail

cd "$(dirname "$0")"


brew_cask_install vim

# Install vundle as Vim package manager
mkdir -p ~/.vim/bundle
test -e ~/.vim/bundle/Vundle.vim || git clone https://github.com/gmarik/Vundle.vim.git ~/.vim/bundle/Vundle.vim

log_once "vim install is finished..."
