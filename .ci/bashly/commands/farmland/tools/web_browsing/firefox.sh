#!/usr/bin/env bash

set -euo pipefail

cd "$(dirname "$0")"


if is_mac_os; then
  brew_install defaultbrowser
  brew_cask_install firefox

  defaultbrowser firefox
elif is_linux_os; then
  log_once "firefox install not yet setup for linux..."
fi

log_once "firefox install is finished..."
