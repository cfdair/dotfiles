#!/usr/bin/env bash

set -euo pipefail

cd "$(dirname "$0")"


is_mac_os && brew_cask_install rectangle
is_linux_os && log_once "No Rectangle setup for linux"

log_once "rectangle install is finished..."
