#!/usr/bin/env bash

set -euo pipefail

cd "$(dirname "$0")"


brew_cask_install flux

log_once "flux install is finished..."
