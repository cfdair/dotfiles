#!/usr/bin/env bash

set -euo pipefail

cd "$(dirname "$0")"


is_mac_os && brew_cask_install steam
is_linux_os && log_once "No Steam setup for linux"

log_once "steam install is finished..."
