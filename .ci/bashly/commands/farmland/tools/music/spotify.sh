#!/usr/bin/env bash

set -euo pipefail

cd "$(dirname "$0")"


if is_mac_os; then
  brew_cask_install spotify
elif is_linux_os; then
  log_once "Spotify not yet setup for linux..."
fi

log_once "spotify install is finished..."
