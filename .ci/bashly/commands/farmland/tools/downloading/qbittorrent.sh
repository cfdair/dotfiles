#!/usr/bin/env bash

set -euo pipefail

cd "$(dirname "$0")"


is_mac_os && brew_cask_install qbittorrent
is_linux_os && log_once "No torrent setup for linux"

log_once "qbittorrent install is finished..."
