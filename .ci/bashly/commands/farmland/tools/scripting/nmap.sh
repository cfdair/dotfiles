#!/usr/bin/env bash

set -euo pipefail

cd "$(dirname "$0")"


brew_install nmap

log_once "nmap install is finished..."
