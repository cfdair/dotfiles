#!/usr/bin/env bash

set -euo pipefail

cd "$(dirname "$0")"


brew_install curl
brew_install wget
brew_install jq

log_once "scripting tools install is finished..."
