#!/usr/bin/env bash

set -euo pipefail

cd "$(dirname "$0")"


is_mac_os && brew_cask_install blender
is_linux_os && log_once "No 3d modeling setup for linux"

log_once "blender install is finished..."
