#!/usr/bin/env bash

set -euo pipefail

cd "$(dirname "$0")"

function allow_iterm2_open {
  # Change permissions on iterm app
  log_once "Allowing the iterm app to be opened"
  spctl --add /Applications/iTerm.app/ || :
}


if is_mac_os; then
  if ! brew_cask_exists iterm2; then
    brew_cask_install iterm2
    allow_iterm2_open
  fi
fi
is_linux_os && log_once "No terminal setup for Linux"

log_once "iterm2 install is finished..."
