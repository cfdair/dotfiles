#!/usr/bin/env bash

set -euo pipefail

cd "$(dirname "$0")"

is_mac_os && brew_cask_install warp
is_linux_os && log_once "No terminal setup for Linux"

log_once "warp install is finished..."
