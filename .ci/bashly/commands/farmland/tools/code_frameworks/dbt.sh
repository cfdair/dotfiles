#!/usr/bin/env bash

set -euo pipefail

cd "$(dirname "$0")"


mkdir -p "~/${XDG_CONFIG_HOME}/dbt"

log_once "dbt install is finished..."
