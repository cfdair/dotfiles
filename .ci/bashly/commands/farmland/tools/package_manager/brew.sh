#!/usr/bin/env bash

set -euo pipefail

cd "$(dirname "$0")"


if test ! "$(command -v brew)"; then
  log_once "Installing homebrew"
  /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
  if is_mac_os; then
    brew tap domt4/autoupdate
    brew autoupdate start 43200 --upgrade --cleanup --immediate --sudo
  fi
fi

log_once "brew install is finished..."
