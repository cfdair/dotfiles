#!/usr/bin/env bash

set -euo pipefail

cd "$(dirname "$0")"


is_mac_os && brew_cask_install telegram
is_linux_os && log_once "No Telegram setup for linux"

log_once "telegram install is finished..."
