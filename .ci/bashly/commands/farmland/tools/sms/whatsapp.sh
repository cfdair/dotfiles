#!/usr/bin/env bash

set -euo pipefail

cd "$(dirname "$0")"


is_mac_os && brew_cask_install whatsapp
is_linux_os && log_once "No Whatsapp setup for linux"

log_once "whatsapp install is finished..."
