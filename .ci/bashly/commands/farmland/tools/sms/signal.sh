#!/usr/bin/env bash

set -euo pipefail

cd "$(dirname "$0")"


is_mac_os && brew_cask_install signal
is_linux_os && log_once "No Signal setup for linux"

log_once "signal install is finished..."
