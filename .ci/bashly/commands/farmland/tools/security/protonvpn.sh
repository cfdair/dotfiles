#!/usr/bin/env bash

set -euo pipefail

cd "$(dirname "$0")"


is_mac_os && brew_cask_install protonvpn
is_linux_os && log_once "No VPN setup for linux"

log_once "ProtonVPN install is finished..."
