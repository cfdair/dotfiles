#!/usr/bin/env bash

set -euo pipefail

cd "$(dirname "$0")"


brew_install nextdns

log_once "next DNS install is finished..."
