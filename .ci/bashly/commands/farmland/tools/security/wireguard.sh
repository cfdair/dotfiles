#!/usr/bin/env bash

set -euo pipefail

cd "$(dirname "$0")"


brew_install wireguard-tools

log_once "wireguard install is finished..."
