#!/usr/bin/env bash

set -euo pipefail

cd "$(dirname "$0")"


if is_mac_os; then
  brew_cask_install 1password
  brew_cask_install 1password-cli
elif is_linux_os; then
  log_once "1password linux install not yet setup..."
fi

log_once "1password install is finished..."
