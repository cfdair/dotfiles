#!/usr/bin/env bash

set -euo pipefail

cd "$(dirname "$0")"


if is_mac_os; then
  brew_cask_install mullvad
elif is_linux_os; then
  log_once "mullvad linux install not yet setup..."
fi

log_once "mullvad install is finished..."
