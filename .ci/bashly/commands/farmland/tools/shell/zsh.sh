#!/usr/bin/env bash

set -euo pipefail

cd "$(dirname "$0")"


log_once "Installing zsh..."

brew_install zsh
brew_install zsh-completions
rm -f ~/.zcompdump; compinit 2> /dev/null || :
brew_install zsh-syntax-highlighting
brew_install zsh-autosuggestions
brew_install z
# Install fzf
# TODO: Set this somewhere as a variable and use it here.
if [[ -d "${XDG_DATA_HOME}/fzf" ]]; then
  :
else
  git clone --depth 1 https://github.com/junegunn/fzf.git $XDG_DATA_HOME/fzf
  $XDG_DATA_HOME/fzf/install --xdg
fi
brew_install the_silver_searcher

# TODO: Pull this in from some source of truth
export ZSH="${XDG_CONFIG_HOME}/oh-my-zsh"

# Change the default shell to zsh
zsh_path="$( command -v zsh )"
if ! grep "$zsh_path" /etc/shells; then
    log_once "adding $zsh_path to /etc/shells"
    echo "$zsh_path" | sudo tee -a /etc/shells
    chsh -s "$zsh_path"
    log_once "default shell changed to $zsh_path"
fi

[[ -d "${ZSH}" ]] || bash -c "$(RUNZSH=no wget -qO- https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" --unattended

# Ensuring log dir exists
mkdir -p "${XDG_DATA_HOME}/share/zsh"

log_once "Successfully installed zsh..."
