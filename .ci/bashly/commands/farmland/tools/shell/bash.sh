#!/usr/bin/env bash

set -euo pipefail

cd "$(dirname "$0")"


brew_install bash

touch ~/.bash_profile

log_once "bash install is finished..."
