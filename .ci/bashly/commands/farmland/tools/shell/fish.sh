#!/usr/bin/env bash

set -euo pipefail

cd "$(dirname "$0")"


brew_install fish
brew_install grc

fish -c 'curl -sL https://raw.githubusercontent.com/jorgebucaran/fisher/main/functions/fisher.fish | source && fisher install jorgebucaran/fisher'
fish -c 'fisher update'

# Install fonts
brew tap homebrew/cask-fonts
brew install font-hack-nerd-font
brew install --cask font-meslo-lg-nerd-font

log_once "fish install is finished..."
