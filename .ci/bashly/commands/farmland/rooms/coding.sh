#!/usr/bin/env bash

set -euo pipefail

cd "$(dirname "$0")"


log_once "Adding a coding room..."

ci_farmland_tools_sms_signal_command
ci_farmland_tools_code_management_git_command
ci_farmland_tools_code_management_gitlab_command
ci_farmland_tools_code_management_nnn_command
ci_farmland_tools_code_management_tmux_command
ci_farmland_tools_dotfiles_stow_command
ci_farmland_tools_scripting_base_command
ci_farmland_tools_shell_bash_command
ci_farmland_tools_shell_fonts_command
ci_farmland_tools_shell_zsh_command
ci_farmland_tools_terminals_iterm2_command
ci_farmland_tools_terminals_warp_command
ci_farmland_tools_text_editor_helix_command
ci_farmland_tools_vm_docker_command
