#!/usr/bin/env bash

set -euo pipefail

cd "$(dirname "$0")"


echo "Building the Royal Golang Engineering Building..."

ci_farmland_rooms_coding_command
ci_farmland_tools_code_language_golang_command
