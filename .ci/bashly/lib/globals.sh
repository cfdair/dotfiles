function brew_install() {
  source .ci/bashly/commands/farmland/tools/package_manager/brew.sh
  local formula=$1
  test "${BREW_PACKAGE_CACHE+x}" || BREW_PACKAGE_CACHE="$(brew list -l | awk '{print $9}')"
  if echo ${BREW_PACKAGE_CACHE} | grep -q "[ ^]${formula}[ $]"; then
      :
  else
    while true; do
        read -p "Install $formula? " yn
        case $yn in
            [Yy]* ) echo "Installing $formula..."; brew install $formula; break;;
            [Nn]* ) break;;
            * ) echo "Please answer yes or no.";;
       esac
    done
  fi
}

function brew_cask_exists {
  local formula=$1
  test "${BREW_PACKAGE_CACHE+x}" || BREW_PACKAGE_CACHE="$(brew list -l | awk '{print $9}')"
  if echo ${BREW_PACKAGE_CACHE} | grep -q "[ ^]${formula}[ $]"; then
    true
  else
    false
  fi
}

function brew_cask_install() {
  source .ci/bashly/commands/farmland/tools/package_manager/brew.sh
  local formula=$1
  test "${BREW_PACKAGE_CACHE+x}" || BREW_PACKAGE_CACHE="$(brew list -l | awk '{print $9}')"
  if echo ${BREW_PACKAGE_CACHE} | grep -q "[ ^]${formula}[ $]"; then
      :
  else
    while true; do
        read -p "Install $formula? " yn
        case $yn in
            [Yy]* ) echo "Installing $formula..."; brew install --cask $formula; break;;
            [Nn]* ) break;;
            * ) echo "Please answer yes or no.";;
       esac
    done
  fi
}

function is_linux_os() {
  [[ $(uname) == "Linux" ]]
}

function is_mac_os() {
  [[ $(uname) == "Darwin" ]]
}

LOGS=
function log_once {
  log="${1}"
  if [[ "${LOGS}" =~ "${log}" ]]; then
    :
  else
    echo "${log}"
    LOGS="${LOGS}:${log}"
  fi
}

# Don't update homebrew during setup
export HOMEBREW_NO_AUTO_UPDATE=true
export HOMEBREW_NO_INSTALL_CLEANUP=true
