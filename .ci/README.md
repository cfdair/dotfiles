# CI

This contains the configuration and implementation of scripts using to 
construct the ci script in the root of the repository.

The ideal here is to try to ensure that what is run in the CI pipeline can also be
run on a local work station.

This is make the developer experience of developing the CI pipeline
and developing in general as smooth as possible.

## Prerequisites

 - docker
 - docker-compose
 - direnv

## Setup

In the root of the directory, run:
```commandline
direnv allow
```

Now bashly should be runnable as a docker image, by running: 
```commandline
bashly --help
``` 

To instruct bashly to generate the `ci` cli, run:
```commandline
bashly generate
``` 

To instruct bashly to generate the `ci` cli whenever something changes, run:
```commandline
bashly generate --watch --quiet
```


## Approach

Where reasonable, all tools are exposed such that they be easily executed straight from
the commandline.

What is added here, is a `ci` cli for simplicity, that is totally optional.

Its goals are to be simple, extensible, easy-to-understand/grep/iterate, and also to 
execute all the commands that are run in the CI pipeline.

It will be runnable within docker, and also on bare metal should that be desirable for the 
developer.

Each important element of the ci script will be printed to script to allow visibility on what
bash is being executed.

## Technologies

### CI pipeline

The pipeline itself uses these technologies:

- dbt
- docker
- python
- terraform
- awscli

### Use of CI technologies

There are multiple tools in use, and those tools will _not_ be abstracted away, but rather 
will remain accessible, and completely usable without any of this CI tooling.

As such, this tooling will remain optional for all tools when developing locally.

### CI tooling technologies

The development of the CI tooling here uses the following technologies:

- bashly
- direnv

### Use of CI tooling technologies

#### Bashly

[Bashly](https://bashly.dannyb.co/) is a bash cli compile tool.

It is a ruby package that builds a single executable bash cli.

Bashly takes a set of simple bash scripts, and a yaml
file describing the cli, and bashly will compile these
into a single executable script.

This means the only dependency to run this ci cli is bash(version > 3.0).

#### direnv

[direnv](https://direnv.net/) is a environment variable loader.

It loads the set of environment variables defined in the `.envrc` file in a directory,
by running:

```commandline
direnv allow
```

This will mean whenever re-entering into this directory, the `.envrc` will be loaded into
the shell session.

## Docs

Find the docs detailing the ci script and its commands and options.

To generate the docs, from the root of the repository, run:
```commandline
bashly render :markdown .ci/docs
```
