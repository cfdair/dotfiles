WORK_DIR="${HOME}/Documents/development/code/seek/"

# Work directory
alias w="cd ${WORK_DIR}"

function aws_as_ {
  "${WORK_DIR}/aws-auth-bash/auth.sh" "$@"; [[ -r "$HOME/.aws/sessiontoken" ]] && . "$HOME/.aws/sessiontoken";
}

# export the GPG tty such that commits can be signed on this seek laptop
export GPG_TTY=$(tty)
