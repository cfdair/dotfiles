source ~/.bash_profile

plugins=(z git ruby fzf)

source ~/.dotfiles/config/config/zsh/xdg.zsh
source ~/.dotfiles/config/config/zsh/env.zsh

# TODO: Change this to load from relative directory
# source all .zsh files inside of the zsh/ directory
source ~/.config/zsh/aws.zsh
source ~/.config/zsh/dbt.zsh
source ~/.config/zsh/direnv.zsh
source ~/.config/zsh/exe.zsh
source ~/.config/zsh/aliases.zsh
source ~/.config/zsh/functions.zsh
source ~/.config/zsh/fzf.zsh
source ~/.config/zsh/git.zsh
source ~/.config/zsh/helix.zsh
source ~/.config/zsh/linux.zsh
source ~/.config/zsh/locale.zsh
source ~/.config/zsh/netskope.zsh
source ~/.config/zsh/python.zsh
source ~/.config/zsh/mise.zsh
source ~/.config/zsh/tmux.zsh
source ~/.config/zsh/work.seek.zsh
source ~/.config/zsh/z.zsh

export AWS_CONFIG_FILE="$XDG_CONFIG_HOME/aws/config"
export AWS_CLI_HISTORY_FILE="$XDG_DATA_HOME/aws/history"
export AWS_CREDENTIALS_FILE="$XDG_DATA_HOME/aws/credentials"
export AWS_WEB_IDENTITY_TOKEN_FILE="$XDG_DATA_HOME/aws/token"
export AWS_SHARED_CREDENTIALS_FILE="$XDG_DATA_HOME/aws/shared-credentials"

# For some reason terraform doesn't like the AWS_WEB_IDENTITY_TOKEN_FILE variable, causing it to throw this: WebIdentityErr: role ARN is not set - so you'd better not export it if not necessary.

source $ZSH/oh-my-zsh.sh

HISTSIZE=1000000
HISTFILESIZE=2000000

[ -f "${XDG_CONFIG_HOME:-$HOME/.config}"/fzf/fzf.zsh ] && source "${XDG_CONFIG_HOME:-$HOME/.config}"/fzf/fzf.zsh
