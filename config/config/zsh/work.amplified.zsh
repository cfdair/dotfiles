# Add elixir binaries to path
export PATH=/Users/$USER/.asdf/installs/elixir/1.9.4-otp-22/bin:$PATH

# Add erlang binaries to path
export PATH=/Users/$USER/.asdf/installs/erlang/22.1.8/bin:$PATH

# Add paths for whorf
export PATH=/usr/local/opt/openssl@1.1/bin:$PATH
export LDFLAGS="-L/usr/local/opt/openssl@1.1/lib"
export CPPFLAGS="-I/usr/local/opt/openssl@1.1/include"
export KERL_CONFIGURE_OPTIONS="--without-javac"

# Work directory
alias w="cd ~/Documents/development/code/work/amplified_ai"

# Work IDEs
alias aw="w; atom ."
alias cw="w; code ."

# aws-vault convenience
export AWS_VAULT_KEYCHAIN_NAME=login

function aws_as_() {
  aws-vault exec "amp_$1" -- ${@:2}
}

function aws_login_as_ {
  aws-vault login "amp_$1" --path="$2"
}
