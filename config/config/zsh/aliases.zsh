# Python utils
alias so="source .venv/bin/activate"

# Miscellaneous Directories
alias infra="cd ~/Documents/development/infra"

# Aliases Other
alias pbc="tr -d \"\n\" | pbcopy"
alias fuck='sudo $(history -p \!\!)'
alias pt="ping google.com"
if type "exa" > /dev/null; then
  alias ll="exa -l -g --icons"
  alias ls="exa --icons"
  alias lt="exa --tree --icons -a -I '.git|__pycache__|.mypy_cache'"
fi

alias fp="fzf --preview 'bat --style=numbers --color=always --line-range :500 {}'"

# Networking
alias dip="curl -s https://iplocation.com/ | grep -E '<b class=\"ip\">|<span class=\"region_name\">|<span class=\"country_name\">' | cut -d '>' -f 3 | cut -d '<' -f 1"

alias cdgo='cd $GOPATH/src/github.com/cfalcondair/'

# Tools
alias mux=tmuxinator

# Programs
alias browse="open -a /Applications/Firefox.app"

# Globals
alias -g G="| grep "
alias -g CC="| pbcopy "
alias -g "?"="| fzf "
alias -g X="| xargs "

# Infra
alias tf="terraform"
