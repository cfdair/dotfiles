# If you come from bash you might have to change your $PATH.
export PATH=$HOME/bin:$PATH
export PATH=/usr/local/bin:$PATH
export PATH=$HOME/.local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH="${XDG_CONFIG_HOME}/oh-my-zsh"

# Add sbin to path
export PATH="/usr/local/sbin:$PATH"

# Coloring
export TERM=xterm-color
# export GREP_OPTIONS='--color=auto' GREP_COLOR='1;32'
export CLICOLOR=1
export LSCOLORS=ExFxCxDxBxegedabagacad

# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="random"
#Standouts =(amuse tjkirch_mod sonicradish terminalparty miloshadzic fishy jbergantine kolo adben garyblessington robbyrussell peepcode emotty)

# Brew
# Opt-out of analytics for homebrew
export HOMEBREW_NO_ANALYTICS=1
# Set how often to update brew
export HOMEBREW_AUTO_UPDATE_SECS="86400"

# Shell settings
export EDITOR=vim
export VISUAL=vim

# docker
export DOCKER_BUILDKIT=0
export COMPOSE_DOCKER_CLI_BUILD=0


# direnv
eval "$(direnv hook zsh)"

