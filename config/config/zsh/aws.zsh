# aws emr list-clusters | jq -r '.Clusters[] | select( .Status.State  == "BOOTSTRAPPING" or .Status.State == "WAITING") | .Id'
#
#
# aws emr describe-cluster --cluster-id j-1G8TD00GNNO5W | jq -r .Cluster.MasterPublicDnsName | sed s/.us-west-2.compute.internal// | awk -F - '{print $2 "." $3 "." $4 "." $5}'
#

function aws_clusters_waiting() {
  aws emr list-clusters | jq -r '.Clusters[] | select(.Status.State == "WAITING") | {name: .Name, id: .Id}'
}

function aws_get_cluster_id() {
  waiting_clusters=$(aws_clusters_waiting)
  if [[ $waiting_clusters ]]; then
    echo $waiting_clusters | jq -r 'select( .name == "$1") | .Id'
  else
    echo "No waiting clusters yet. Will need to wait."
  fi
}

function aws_list_cluster_ips() {
  cluster_id=$(aws_get_cluster_id "$1")
  if [[ $cluster_id ]]; then
    aws emr describe-cluster --cluster-id $cluster_id | \
      jq -r '.Cluster.MasterPublicDnsName' | \
      sed s/.us-west-2.compute.internal// | \
      awk -F - '{print $2 "." $3 "." $4 "." $5}'
  else
    echo "No clusters found with name $1."
  fi
}
