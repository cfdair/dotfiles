HISTSIZE=1000000
HISTFILESIZE=2000000


[ -f "${XDG_CONFIG_HOME:-$HOME/.config}"/fzf/fzf.bash ] && source "${XDG_CONFIG_HOME:-$HOME/.config}"/fzf/fzf.bash
source '/opt/homebrew/opt/autoenv/activate.sh'
