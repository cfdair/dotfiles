# Python requests
export REQUESTS_CA_BUNDLE="$(brew --prefix)/etc/ca-certificates/cert.pem"

# aws-cli
export AWS_CA_BUNDLE="$(brew --prefix)/etc/ca-certificates/cert.pem"

# Python HTTPLib2
export HTTPLIB2_CA_CERTS="$(brew --prefix)/etc/ca-certificates/cert.pem"

# Node.JS
export NODE_EXTRA_CA_CERTS="$(brew --prefix)/etc/ca-certificates/cert.pem"

# Ruby custom build support
export SSL_CERT_FILE="$(brew --prefix)/etc/ca-certificates/cert.pem"
export RUBY_CONFIGURE_OPTS="--with-openssl-dir=$(brew --prefix openssl@1.1)"

# Used for DBT
export CURL_CA_BUNDLE="$(brew --prefix)/etc/ca-certificates/cert.pem"
